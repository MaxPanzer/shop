// LoginResult.swift
// Copyright © VVP. All rights reserved.

import Foundation

/// LoginResult model
struct LoginResult: Codable {
    let result: Int
    let user: User
}
