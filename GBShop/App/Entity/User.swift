// User.swift
// Copyright © VVP. All rights reserved.

import Foundation

/// User model
struct User: Codable {
    var id: Int
    var login: String
    var name: String
    var lastname: String

    enum CodingKeys: String, CodingKey {
        case id = "id_user"
        case login = "user_login"
        case name = "user_name"
        case lastname = "user_lastname"
    }
}
