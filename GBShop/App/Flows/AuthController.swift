// AuthController.swift
// Copyright © VVP. All rights reserved.

import UIKit

final class AuthController: UIViewController {
    var requestFactory: RequestFactory?

    override func viewDidLoad() {
        super.viewDidLoad()

        let auth = requestFactory?.makeAuthRequestFactory()
        auth?.login(userName: "Somebody", password: "mypassword") { response in
            switch response.result {
            case let .success(login):
                print(login)
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }
}

//
