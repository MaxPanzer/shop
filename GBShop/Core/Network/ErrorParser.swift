// ErrorParser.swift
// Copyright © VVP. All rights reserved.

import Foundation

final class ErrorParser: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        result
    }

    func parse(response _: HTTPURLResponse?, data _: Data?, error: Error?) -> Error? {
        error
    }
}
