// Auth.swift
// Copyright © VVP. All rights reserved.

import Alamofire
import Foundation

final class Auth: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: Session
    var queue: DispatchQueue
    let baseURL = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")

    init(
        errorParser: AbstractErrorParser,
        sessionManager: Session,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)
    ) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue ?? DispatchQueue.global(qos: .utility)
    }
}

extension Auth: AuthRequestFactory {
    func login(userName: String, password: String, completion: @escaping (AFDataResponse<LoginResult>) -> Void) {
        guard let baseURL = baseURL else { return }
        let requestModel = Login(baseURL: baseURL, login: userName, password: password)
        request(request: requestModel, completion: completion)
    }
}

extension Auth {
    struct Login: RequestRouter {
        var baseURL: URL
        let method: HTTPMethod = .get
        let path: String = "login.json"
        let login: String
        let password: String
        var parameters: Parameters? {
            [
                "username": login,
                "password": password,
            ]
        }
    }
}
