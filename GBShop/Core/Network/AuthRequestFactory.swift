// AuthRequestFactory.swift
// Copyright © VVP. All rights reserved.

import Alamofire
import Foundation

protocol AuthRequestFactory {
    func login(userName: String, password: String, completion: @escaping (AFDataResponse<LoginResult>) -> Void)
}
