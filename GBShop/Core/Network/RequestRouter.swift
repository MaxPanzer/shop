// RequestRouter.swift
// Copyright © VVP. All rights reserved.

import Alamofire
import Foundation

/// RequestRouterEncoding
enum RequestRouterEncoding {
    case url, json
}

protocol RequestRouter: URLRequestConvertible {
    var baseURL: URL { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var fullURL: URL { get }
    var encoding: RequestRouterEncoding { get }
}

/// RequestRouter
extension RequestRouter {
    var fullURL: URL {
        baseURL.appendingPathComponent(path)
    }

    var encoding: RequestRouterEncoding {
        .url
    }

    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: fullURL)
        urlRequest.httpMethod = method.rawValue

        switch encoding {
        case .url:
            return try URLEncoding.default.encode(urlRequest, with: parameters)
        case .json:
            return try JSONEncoding.default.encode(urlRequest, with: parameters)
        }
    }
}
