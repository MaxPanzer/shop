// RequestFactory.swift
// Copyright © VVP. All rights reserved.

import Alamofire
import Foundation

final class RequestFactory {
    func makeErrorParser() -> AbstractErrorParser {
        ErrorParser()
    }

    lazy var commonSessionManager: Session = {
        let config = URLSessionConfiguration.default
        config.httpShouldSetCookies = false
        config.httpAdditionalHeaders = HTTPHeaders.default.dictionary
        let manager = Session(configuration: config)
        return manager
    }()

    let sessionQueue = DispatchQueue.global(qos: .utility)

    func makeAuthRequestFactory() -> AuthRequestFactory {
        let errorParser = makeErrorParser()
        return Auth(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
}
