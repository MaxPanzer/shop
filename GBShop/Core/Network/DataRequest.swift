// DataRequest.swift
// Copyright © VVP. All rights reserved.

import Alamofire
import Foundation

extension DataRequest {
    @discardableResult
    func responseCodable<T: Decodable>(
        errorParser: AbstractErrorParser,
        queue: DispatchQueue = .main,
        completionHandler: @escaping (AFDataResponse<T>) -> Void
    )
        -> Self
    {
        let responseSerializer = DecodableSerializer<T>(errorParser: errorParser)
        return response(queue: queue, responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}

protocol AbstractRequestFactory {
    var errorParser: AbstractErrorParser { get }
    var sessionManager: Session { get }
    var queue: DispatchQueue { get }

    @discardableResult
    func request<T: Decodable>(
        request: URLRequestConvertible,
        completion: @escaping (AFDataResponse<T>) -> Void
    ) -> DataRequest
}

extension AbstractRequestFactory {
    @discardableResult
    func request<T: Decodable>(
        request: URLRequestConvertible,
        completion: @escaping (AFDataResponse<T>) -> Void
    ) -> DataRequest {
        sessionManager
            .request(request)
            .responseCodable(errorParser: errorParser, queue: queue, completionHandler: completion)
    }
}
