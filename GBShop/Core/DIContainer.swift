// DIContainer.swift
// Copyright © VVP. All rights reserved.

import Foundation
import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard {
    class func setup() {
        defaultContainer.storyboardInitCompleted(AuthController.self) { resolver, container in
            container.requestFactory = resolver.resolve(RequestFactory.self)
        }

        defaultContainer.storyboardInitCompleted(RegisterController.self) { resolver, container in
            container.requestFactory = resolver.resolve(RequestFactory.self)
        }

        defaultContainer.storyboardInitCompleted(ExitController.self) { resolver, container in
            container.requestFactory = resolver.resolve(RequestFactory.self)
        }

        defaultContainer.storyboardInitCompleted(ChangeDataController.self) { resolver, container in
            container.requestFactory = resolver.resolve(RequestFactory.self)
        }

        defaultContainer.register(RequestFactory.self) { _ in
            RequestFactory()
        }
    }
}
